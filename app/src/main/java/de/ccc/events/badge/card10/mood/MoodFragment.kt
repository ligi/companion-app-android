/*
 * Copyright by the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.ccc.events.badge.card10.mood

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothGatt
import android.bluetooth.BluetoothGattCallback
import android.bluetooth.BluetoothGattCharacteristic
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import de.ccc.events.badge.card10.CARD10_BLUETOOTH_MAC_PREFIX
import de.ccc.events.badge.card10.CARD10_SERVICE_UUID
import de.ccc.events.badge.card10.R
import de.ccc.events.badge.card10.ROCKETS_CHARACTERISTIC_UUID
import kotlinx.android.synthetic.main.mood_fragment.*
import java.util.concurrent.CountDownLatch

class MoodFragment : Fragment() {

    private val bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
    private lateinit var gatt: BluetoothGatt
    private var rocketsCharacteristic: BluetoothGattCharacteristic? = null
    private var writeLatch: CountDownLatch? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val callback = object : BluetoothGattCallback() {
            override fun onConnectionStateChange(gatt: BluetoothGatt, status: Int, newState: Int) {
                System.out.println("===== onConnectionStateChange " + gatt + " / " + status + " / " + newState)
                if (newState == BluetoothGatt.STATE_CONNECTED)
                    gatt.discoverServices()
            }

            override fun onServicesDiscovered(gatt: BluetoothGatt, status: Int) {
                System.out.println("===== onServicesDiscovered " + gatt + " / " + status)
                val card10Service = gatt.getService(CARD10_SERVICE_UUID)
                rocketsCharacteristic = card10Service.getCharacteristic(ROCKETS_CHARACTERISTIC_UUID)
            }

            override fun onCharacteristicWrite(
                gatt: BluetoothGatt,
                characteristic: BluetoothGattCharacteristic,
                status: Int
            ) {
                System.out.println("===== onCharacteristicWrite " + characteristic.uuid.toString() + " / " + characteristic.value + " / " + status)
                writeLatch?.countDown();
            }
        }

        val remoteDevices =
            bluetoothAdapter.bondedDevices.filter { it.address.startsWith(CARD10_BLUETOOTH_MAC_PREFIX, true) }
        if (remoteDevices.isEmpty())
            activity!!.finish()
        gatt = remoteDevices.get(0).connectGatt(activity, false, callback)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.mood_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        mood_good.setOnClickListener({
            writeGatt(rocketsCharacteristic!!, ubyteArrayOf(0xffu, 0x00u, 0x00u).toByteArray())
        })
        mood_neutral.setOnClickListener({
            writeGatt(rocketsCharacteristic!!, ubyteArrayOf(0x00u, 0xffu, 0x00u).toByteArray())
        })
        mood_bad.setOnClickListener({
            writeGatt(rocketsCharacteristic!!, ubyteArrayOf(0x00u, 0x00u, 0xffu).toByteArray())
        })
    }

    fun writeGatt(characteristic: BluetoothGattCharacteristic, values: ByteArray) {
        characteristic.value = values
        val init = gatt.writeCharacteristic(rocketsCharacteristic)
        if (!init)
            System.out.println("Failed to initiate writing GATT attribute")
        writeLatch = CountDownLatch(1)
        writeLatch!!.await();
    }

    override fun onDestroy() {
        gatt.close();
        super.onDestroy()
    }
}
