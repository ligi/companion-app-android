/*
 * Copyright by the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.ccc.events.badge.card10.filetransfer

import android.bluetooth.BluetoothGatt
import android.bluetooth.BluetoothGattCharacteristic
import android.bluetooth.BluetoothGattService
import android.util.Log
import de.ccc.events.badge.card10.common.ConnectionService
import de.ccc.events.badge.card10.common.GattListener
import de.ccc.events.badge.card10.filetransfer.protocol.Packet
import java.util.*

private const val TAG = "LowEffortService"

class LowEffortService(
    service: BluetoothGattService
) : GattListener {
    private val centralTx: BluetoothGattCharacteristic
    private val centralRx: BluetoothGattCharacteristic

    private val centralTxCharacteristicUuid = UUID.fromString("42230101-2342-2342-2342-234223422342")
    private val centralRxCharacteristicUuid = UUID.fromString("42230102-2342-2342-2342-234223422342")

    private var notifyEnabled = false
    private val listeners = mutableListOf<OnPacketReceivedListener>()

    init {
        val tx = service.getCharacteristic(centralTxCharacteristicUuid)
        val rx = service.getCharacteristic(centralRxCharacteristicUuid)

        if (tx == null || rx == null) {
            throw IllegalStateException()
        }

        tx.writeType = BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE

        centralTx = tx
        centralRx = rx

        ConnectionService.addGattListener(this)
    }

    fun enableNotify(gatt: BluetoothGatt) {
        if (notifyEnabled) {
            return
        }

        val notifySuccess = gatt.setCharacteristicNotification(centralRx, true)
        if (!notifySuccess) {
            Log.e(TAG, "Notify enable failed")
        } else {
            notifyEnabled = true
        }
    }

    fun sendPacket(packet: Packet): Boolean {
//        Thread.sleep(100)

        val bytes = packet.getBytes()

        centralTx.value = bytes
        val status = ConnectionService.writeCharacteristic(centralTx)

        if (!status) {
            Log.d(TAG, "Write status: $status")
        }

        return status
    }

    fun addOnPacketReceivedListener(listener: OnPacketReceivedListener) {
        listeners.add(listener)
    }

    // GattListener methods
    override fun onCharacteristicWrite(characteristic: BluetoothGattCharacteristic, status: Int) {
        Log.d(TAG, "onCharacteristicWrite: $status")
    }

    override fun onCharacteristicChanged(characteristic: BluetoothGattCharacteristic) {
        listeners.map { it.onPacketReceived(Packet.fromBytes(characteristic.value)) }
    }
}